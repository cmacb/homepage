import time
import os

from .override import SeleniumTestCase


from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.common.exceptions import WebDriverException

MAX_WAIT = 10


class FunctionalTest(SeleniumTestCase):

    def setUp(self):
        staging_server = os.environ.get('STAGING_SERVER')
        self.firefox = webdriver.Remote(
            command_executor='http://selenium:4444/wd/hub',
            desired_capabilities=DesiredCapabilities.FIREFOX
        )
        if staging_server:
            self.live_server_url = "http://" + staging_server

    def tearDown(self):
        self.firefox.quit()

    def wait_for_row_in_list(self, row_text):
        start_time = time.time()
        while True:
            try:
                table = self.firefox.find_element_by_id("blog_posts")
                rows = table.find_elements_by_tag_name('li')
                self.assertIn(row_text, [row.text for row in rows])
                return
            except(AssertionError, WebDriverException) as e:
                if time.time() - start_time > MAX_WAIT:
                    raise e

                time.sleep(0.5)

    def wait_for(self, fn):
        start_time = time.time()
        while True:
            try:
                return fn()
            except (AssertionError, WebDriverException) as e:
                if time.time() - start_time > MAX_WAIT:
                    raise e
                time.sleep(0.5)

    def get_item_input_box(self):
        return self.firefox.find_element_by_id('id_text')
