from django import forms
from blog.models import Post
from django.core.exceptions import ValidationError

EMPTY_POST_ERROR = "You can't have an empty blog post"
DUPLICATE_POST_ERROR = "You've already got this in your blog"


class PostForm(forms.models.ModelForm):

    class Meta:
        model = Post
        fields = ('text',)
        widgets = {
            'text': forms.fields.TextInput(attrs={
                'placeholder': 'Enter some text ya dummy',
                'class': 'form-control input-lg',
            }),
        }
        error_messages = {
            'text': {'required': "You can't have an empty blog post"}
        }

    def save(self, for_blog):
        self.instance.blog = for_blog
        return super().save()


class ExistingBlogPostForm(PostForm):
    def __init__(self, for_blog, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.instance.blog = for_blog

    def validate_unique(self):
        try:
            self.instance.validate_unique()
        except ValidationError as e:
            e.error_dict = {'text': [DUPLICATE_POST_ERROR]}
            self._update_errors(e)

    def save(self):
        return forms.models.ModelForm.save(self)
