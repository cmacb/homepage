from django.test import TestCase
from django.utils.html import escape
from blog.models import Blog, Post
from blog.forms import PostForm, EMPTY_POST_ERROR, DUPLICATE_POST_ERROR, ExistingBlogPostForm


class BlogViewTest(TestCase):

    def post_invalid_input(self):
        blog = Blog.objects.create()
        return self.client.post(
            f'/blog/{blog.id}/',
            data={'text': ''}
        )

    def test_for_invalid_input_nothing_saved_to_db(self):
        self.post_invalid_input()
        self.assertEqual(Post.objects.count(), 0)

    def test_for_invalid_input_renders_post_template(self):
        response = self.post_invalid_input()
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'post_list.html')

    def test_for_invalid_input_passes_form_to_template(self):
        response = self.post_invalid_input()
        self.assertIsInstance(response.context['form'], PostForm)

    def test_for_invalid_input_shows_error_on_page(self):
        response = self.post_invalid_input()
        self.assertContains(response, escape(EMPTY_POST_ERROR))

    def test_displays_only_posts_for_that_blog(self):
        blog = Blog.objects.create()
        Post.objects.create(text='itemy 1', blog=blog)
        Post.objects.create(text='itemy 2', blog=blog)

        other_blog = Blog.objects.create()
        Post.objects.create(text='other 1', blog=other_blog)
        Post.objects.create(text='other 2', blog=other_blog)

        response = self.client.get(f'/blog/{blog.id}/')

        self.assertContains(response, 'itemy 1')
        self.assertContains(response, 'itemy 2')

        self.assertNotContains(response, 'other 1')
        self.assertNotContains(response, 'other 2')

    def test_uses_post_template(self):
        blog = Blog.objects.create()
        response = self.client.get(f'/blog/{blog.id}/')
        self.assertTemplateUsed(response, 'post_list.html')

    def test_passes_correct_list_to_template(self):
        correct_list = Blog.objects.create()
        response = self.client.get(f'/blog/{correct_list.id}/')
        self.assertEqual(response.context['blog'], correct_list)

    def test_can_save_a_post_request_to_an_existing_list(self):
        correct_list = Blog.objects.create()

        self.client.post(
            f'/blog/{correct_list.id}/',
            data={'text': 'A new tiem for an existing blog'}
        )

        self.assertEqual(Post.objects.count(), 1)
        new_item = Post.objects.first()
        self.assertEqual(new_item.text, 'A new tiem for an existing blog')
        self.assertEqual(new_item.blog, correct_list)

    def test_POST_redirects_to_list_view(self):
        other_list = Blog.objects.create()
        correct_list = Blog.objects.create()

        response = self.client.post(
            f'/blog/{correct_list.id}/',
            data={'text': 'A new tiem for an existing blog'}
        )

        self.assertRedirects(response, f'/blog/{correct_list.id}/')

    def test_validation_errors_end_up_on_blogs_page(self):
        blog = Blog.objects.create()
        response = self.client.post(
            f'/blog/{blog.id}/',
            data={'text': ''}
        )
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'post_list.html')
        expected_error = escape("You can't have an empty blog post")
        self.assertContains(response, expected_error)

    def test_home_page_uses_item_form(self):
        response = self.client.get("/blog/new")
        self.assertIsInstance(response.context["form"], PostForm)

    def test_displays_item_form(self):
        blog = Blog.objects.create()
        response = self.client.get(f'/blog/{blog.id}/')
        self.assertIsInstance(response.context['form'], ExistingBlogPostForm)
        self.assertContains(response, 'name="text"')


class NewBlogTest(TestCase):

    def test_can_save_post_request(self):
        self.client.post('/blog/new', data={"text": "A new post"})
        self.assertEqual(Post.objects.count(), 1)
        new_item = Post.objects.first()
        self.assertEqual(new_item.text, "A new post")

    def test_redirects_after_post(self):
        response = self.client.post('/blog/new', data={"text": "A new post"})
        new_blog = Blog.objects.first()
        self.assertRedirects(response, f'/blog/{new_blog.id}/')

    def test_invalid_input_renders_home_template(self):
        response = self.client.post('/blog/new', data={'text': ''})
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'blog.html')

    def test_validation_errors_end_up_on_blogs_page(self):
        response = self.client.post('/blog/new', data={'text': ''})
        self.assertContains(response, escape(EMPTY_POST_ERROR))

    def test_invalid_input_passes_form_to_template(self):
        response = self.client.post('/blog/new', data={'text': ''})
        self.assertIsInstance(response.context['form'], PostForm)

    def test_invalid_list_items_arent_saved(self):
        self.client.post('/blog/new', data={'text': ''})
        self.assertEqual(Blog.objects.count(), 0)
        self.assertEqual(Post.objects.count(), 0)

    def test_duplicate_item_validation_errors_end_up_on_lists_page(self):
        list1 = Blog.objects.create()
        item1 = Post.objects.create(blog=list1, text='textey')
        response = self.client.post(
            f'/blog/{list1.id}/',
            data={'text': 'textey'}
        )

        expected_error = escape(DUPLICATE_POST_ERROR)
        self.assertContains(response, expected_error)
        self.assertTemplateUsed(response, 'post_list.html')
        self.assertEqual(Post.objects.all().count(), 1)
